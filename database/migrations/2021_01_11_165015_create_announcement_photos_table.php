<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnnouncementPhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('announcement_photos', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('announcement_id')->unsigned();
            $table->bigInteger('photo_id')->unsigned();

            $table->foreign('announcement_id')
                    ->references('id')
                    ->on('announcements')
                    ->onDelete('cascade');

            $table->foreign('photo_id')
                    ->references('id')
                    ->on('photos')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('announcement_photos');
    }
}
