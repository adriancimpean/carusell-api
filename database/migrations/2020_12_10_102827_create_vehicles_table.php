<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicles', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('make_id')->unsigned();
            $table->bigInteger('model_id')->unsigned();
            $table->integer('year');
            $table->integer('kilometers');
            $table->bigInteger('bodyType_id')->unsigned();
            $table->integer('engine_size');
            $table->bigInteger('transmissionType_id')->unsigned();
            $table->float('price');
            $table->string('description');
            $table->integer('power');
            $table->bigInteger('fuelType_id')->unsigned();

            $table->foreign('make_id')
                    ->references('id')
                    ->on('makes')
                    ->onDelete('cascade');

            $table->foreign('model_id')
                    ->references('id')
                    ->on('models')
                    ->onDelete('cascade');

            $table->foreign('bodyType_id')
                    ->references('id')
                    ->on('body_type')
                    ->onDelete('cascade');
            
            $table->foreign('transmissionType_id')
                    ->references('id')
                    ->on('transmission_type')
                    ->onDelete('cascade');

            $table->foreign('fuelType_id')
                    ->references('id')
                    ->on('fuel_type')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicles');
    }
}
