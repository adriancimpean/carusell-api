<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemovePhotoIdFromAnnouncementPhotos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('announcement_photos', function (Blueprint $table) {
            $table->dropForeign(['photo_id']);
            $table->dropColumn('photo_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('announcement_photos', function (Blueprint $table) {
            $table->bigInteger('photo_id')->unsigned();

            $table->foreign('photo_id')
                    ->references('id')
                    ->on('photos')
                    ->onDelete('cascade');
        });
    }
}
