<?php
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['namespace' => 'Api\v1\Auth'], function() {
    Route::post('/login', 'AuthController@login');
    Route::post('/register', 'AuthController@register');
});

Route::group(['namespace' => 'Api\v1'], function() {
    Route::group(['prefix' => 'users'], function(){
        Route::get('/', 'UserController@index');
        Route::get('/user/{email}', 'UserController@show');
        Route::put('/update/{id}', 'UserController@update');
    });

    Route::group(['prefix' => 'makes', 'middleware' => 'auth:api'], function() {
        Route::get('/', 'MakesController@index');
        Route::get('/make/{make}', 'MakesController@show');
    });

    Route::group(['prefix' => 'vehicles', 'middleware' => 'auth:api'], function() {
        Route::get('/', 'VehicleController@index');
        Route::get('/vehicle/{vehicle}', 'VehicleController@show');
        Route::post('/createVehicle', 'VehicleController@store');

        Route::get('/transmissions', 'TransmissionTypesController@index');
        Route::get('/bodyTypes', 'BodyTypesController@index');
        Route::get('/fuelTypes', "FuelTypesController@index");

    });

    Route::group(['prefix' => 'announcements', 'middleware' => 'auth:api'], function() {
        Route::get('/', 'AnnouncementController@index');
        Route::get('/announcement/{announcement}', 'AnnouncementController@show');
        Route::post('/create', 'AnnouncementController@store');
        Route::put('/update/{id}', 'AnnouncementController@update');
        Route::delete('/delete/{id}', "AnnouncementController@delete");
        Route::get('/user/{userId}', 'AnnouncementController@getUserActiveAnnouncements');
        Route::get('/images/{announcementId}', 'AnnouncementPhotosController@getAnnouncementImages');
        Route::post('/addImage', 'AnnouncementPhotosController@store');
        Route::get('/filter', 'AnnouncementController@filterAnnouncements');
        Route::put("/{id}/updateImage", "AnnouncementPhotosController@updateAnnouncementImage");
    });

    Route::group(['prefix' => 'models', 'middleware' => 'auth:api'], function() {
        Route::get('/{makeID}', 'ModelsController@index');
    });

    Route::group(['prefix' => 'cities'], function() {
        Route::get('/', 'CitiesController@index');
    });

    Route::group(['prefix' => 'favorites', 'middleware' => 'auth:api'], function() {
        Route::get('/', 'FavoritesController@index');
        Route::post('/addToFavorites', 'FavoritesController@store');
        Route::get('/{userId}', 'FavoritesController@getUserFavorites');
        Route::delete('/removeFavorite/{favoriteAnnouncementId}', 'FavoritesController@delete');
    });
});


