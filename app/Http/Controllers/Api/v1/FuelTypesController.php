<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\FuelType;

class FuelTypesController extends Controller
{
    public function index(Request $request) {
        return FuelType::all();
    }
}
