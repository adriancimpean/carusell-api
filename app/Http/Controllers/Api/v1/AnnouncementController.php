<?php
namespace App\Http\Controllers\Api\v1;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Announcement;
use App\Models\Vehicle;
use App\Models\Photo;
use Illuminate\Support\Facades\DB;

class AnnouncementController extends Controller
{
    public function index() {
        return DB::table('announcements')
                    ->join('vehicles', 'announcements.vehicle_id', '=', 'vehicles.id')
                    ->join('makes', 'vehicles.make_id', '=', 'makes.id')
                    ->join('cities', 'announcements.city_id', '=', 'cities.id')
                    ->join('users', 'announcements.user_id', '=', 'users.id')
                    ->select('announcements.*', 'makes.name as makeName', 'vehicles.year', 'vehicles.kilometers', 'vehicles.price', 'cities.name as cityName', 'users.first_name', 'users.last_name', 'users.phone', 'users.email')
                    ->get();
    }

    public function show(int $announcement) {
        return DB::table('announcements')
                    ->join('vehicles', 'announcements.vehicle_id', '=', 'vehicles.id')
                    ->join('makes', 'vehicles.make_id', '=', 'makes.id')
                    ->join('cities', 'announcements.city_id', '=', 'cities.id')
                    ->join('users', 'announcements.user_id', '=', 'users.id')
                    ->select('announcements.*', 'makes.name as makeName', 'vehicles.year', 'vehicles.kilometers', 'vehicles.price', 'cities.name as cityName', 'users.first_name', 'users.last_name', 'users.phone', 'users.email')
                    ->where('announcements.id', $announcement)
                    ->get();
    }

    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
            'make_id' => 'required|integer',
            'model_id' => 'required|integer',
            'year' => 'required|integer',
            'kilometers' => 'required|integer',
            'bodyType_id' => 'required|integer',
            'engine_size' => 'required|integer',
            'transmissionType_id' => 'required|integer',
            'price' => 'required|integer',
            'power' => 'required|integer',
            'fuelType_id' => 'required|integer',
            'title' => 'required|string',
            'description' => 'required|string',
            'city_id' => 'required|integer',
            'user_id' => 'required|integer',
         ]);

        if($validator->fails()) {
            return response()->json([
                'error' => 'BAD REQUEST',
                'message' => $validator->errors()->toArray()
            ], 400);
        }

        $vehicle = new Vehicle();
        $vehicle->make_id = request('make_id');
        $vehicle->model_id = request('model_id');
        $vehicle->year = request('year');
        $vehicle->kilometers = request('kilometers');
        $vehicle->bodyType_id = request('bodyType_id');
        $vehicle->engine_size = request('engine_size');
        $vehicle->transmissionType_id = request('transmissionType_id');
        $vehicle->price = request('price');
        $vehicle->description = request('description');
        $vehicle->power = request('power');
        $vehicle->fuelType_id = request('fuelType_id');

        $vehicle->save();

        $announcement = new Announcement();
        $announcement->title = request('title');
        $announcement->description = request('description');
        $announcement->vehicle_id = $vehicle->id;
        $announcement->city_id = request('city_id');
        $announcement->user_id = request('user_id');

        $announcement->save();

        return response()->json([
            'message' => 'Announcement added successfully!',
            'announcement' => $announcement
        ],201);
    }

    public function update(Request $request, $id) {
        $validator = Validator::make($request->all(), [
            'make_id' => 'required|integer',
            'model_id' => 'required|integer',
            'year' => 'required|integer',
            'kilometers' => 'required|integer',
            'bodyType_id' => 'required|integer',
            'engine_size' => 'required|integer',
            'transmissionType_id' => 'required|integer',
            'price' => 'required|integer',
            'power' => 'required|integer',
            'fuelType_id' => 'required|integer',
            'title' => 'required|string',
            'description' => 'required|string',
            'city_id' => 'required|integer',
            'user_id' => 'required|integer',
        ]);

        if($validator->fails()) {
            return response()->json([
                'error' => 'BAD REQUEST',
                'message' => $validator->errors()->toArray()
            ], 400);
        }

        $announcement = Announcement::findOrFail($id);
        $vehicle = Vehicle::findOrFail($announcement->vehicle_id);
        if(!$announcement) {
            return response()->json([
                'message' => 'Announcement not found',
            ], 404);
        }

        if(!$vehicle) {
            return response()->json([
                'message' => 'Vehicle not found',
            ], 404);
        }

        $vehicle->make_id = request('make_id');
        $vehicle->model_id = request('model_id');
        $vehicle->year = request('year');
        $vehicle->kilometers = request('kilometers');
        $vehicle->bodyType_id = request('bodyType_id');
        $vehicle->engine_size = request('engine_size');
        $vehicle->transmissionType_id = request('transmissionType_id');
        $vehicle->price = request('price');
        $vehicle->description = request('description');
        $vehicle->power = request('power');
        $vehicle->fuelType_id = request('fuelType_id');
        $vehicle->save();

        $announcement->title = request('title');
        $announcement->description = request('description');
        $announcement->vehicle_id = $vehicle->id;
        $announcement->city_id = request('city_id');
        $announcement->user_id = request('user_id');
        $announcement->save();

        return response()->json([
            'message' => 'Announcement successfully updated',
            $announcement,
            $vehicle
        ], 200);
    }

    public function delete($id) {
        $announcement = Announcement::findOrFail($id);
        if($announcement) {
            $announcement->delete();
            return response()->json([
                'message' => 'Announcement deleted successfully!'
            ]);
        }
        return response()->json([
            'message' => "Announcement not found!"
        ]);
    }

    public function getUserActiveAnnouncements($userId) {
        return DB::table('announcements')
                    ->join('vehicles', 'announcements.vehicle_id', '=', 'vehicles.id')
                    ->join('makes', 'vehicles.make_id', '=', 'makes.id')
                    ->join('cities', 'announcements.city_id', '=', 'cities.id')
                    ->join('users', 'announcements.user_id', '=', 'users.id')
                    ->select('announcements.*', 'makes.name as makeName', 'vehicles.year', 'vehicles.kilometers', 'vehicles.price', 'cities.name as cityName', 'users.first_name', 'users.last_name', 'users.phone', 'users.email')
                    ->where('announcements.user_id', $userId)
                    ->get();
    }

    public function filterAnnouncements(Request $request) {
        $query = Announcement::query();
        $query->join('vehicles', 'announcements.vehicle_id', '=', 'vehicles.id');
        $query->select('announcements.id as announcement_id', 'announcements.*', 'vehicles.*');
        foreach($request->query() as $key=>$value) {
            if($key == 'price:from') {
                $price_from = $value;
                continue;
            }
            if($key == 'price:to') {
                $price_to = $value;
                continue;
            }
            if($key == 'km:from') {
                $km_from = $value;
                continue;
            }
            if($key == 'km:to') {
                $km_to = $value;
                continue;
            }
            if($key == 'year:from') {
                $year_from = $value;
                continue;
            }
            if($key == 'year:to') {
                $year_to = $value;
                continue;
            }
            if($key == 'text') {
                $query->where('title', 'LIKE' , "%{$value}%")->orWhere('announcements.description', 'LIKE', "%{$value}%");
                continue;
            }
            $query->where($key, $value);
        }
        if(!empty($price_from) && !empty($price_to)) {
            $query->where('price', '>', $price_from)->where('price', '<', $price_to);
        }
        if(!empty($km_from) && !empty($km_to)) {
            $query->where('kilometers', '>', $km_from)->where('kilometers', '<', $km_to);
        }
        if(!empty($year_from) && !empty($year_to)) {
            $query->where('year', '>', $year_from)->where('year', '<', $year_to);
        }
        return $query->get();
    }
}
