<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class ModelsController extends Controller
{
    public function index(int $makeID) {
        return DB::table('models')
                    ->select('models.id', 'models.name')
                    ->join('makes', 'models.make_id', '=', 'makes.id')
                    ->where('models.make_id', $makeID)
                    ->get();
    }
}
