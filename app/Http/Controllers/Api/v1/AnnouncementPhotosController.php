<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use App\Models\AnnouncementPhotos;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class AnnouncementPhotosController extends Controller
{
    public function index(Request $request) {
        return AnnouncementPhotos::all();
    }

    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
            'announcement_id' => 'required|integer',
            'photo_url' => 'required|string',
        ]);

        if($validator->fails()) {
            return response()->json([
                'error' => 'BAD REQUEST',
                'message' => $validator->errors()->toArray()
            ], 400);
        }

        $announcement_photo = new AnnouncementPhotos();
        $announcement_photo->announcement_id = request('announcement_id');
        $announcement_photo->photo_url = request('photo_url');
        $announcement_photo->save();

        return response()->json([
            'message' => 'Photo added successfully!',
            'announcement_photo' => $announcement_photo
        ],201);
    }

    public function getAnnouncementImages($announcementId) {
        return DB::table('announcement_photos')
                ->select('announcement_photos.*')
                ->where('announcement_id', $announcementId)
                ->get();
    }

    public function updateAnnouncementImage(Request $request, $id) {
        $validator = Validator::make($request->all(), [
            'announcement_id' => 'required|integer',
            'photo_url' => 'required|string',
        ]);

        if($validator->fails()) {
            return response()->json([
                'error' => 'BAD REQUEST',
                'message' => $validator->errors()->toArray()
            ], 400);
        }

        $announcement = AnnouncementPhotos::where('announcement_id', '=', $id)->firstOrFail();
        if(!$announcement) {
            return response()->json([
                'message' => 'Announcement not found',
            ], 404);
        }

        $announcement->update($request->all());
        return response()->json([
            'message' => 'Announcement successfully updated',
            $announcement
        ], 200);
    }
}
