<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\Vehicle;
use Illuminate\Support\Facades\DB;


class VehicleController extends Controller
{
    public function index(Request $request) {
        return Vehicle::all();
    }

    public function show(int $vehicle) {
        return DB::table('vehicles')
            ->select('vehicles.id', 'makes.name as make', 'models.name as model', 'vehicles.year', 'vehicles.kilometers', 
                        'body_type.name as bodyType', 'vehicles.engine_size', 'transmission_type.name as transmissionType', 'vehicles.price', 
                        'vehicles.power', 'fuel_type.name as fuel_type')
            ->join('makes', 'vehicles.make_id', '=', 'makes.id')
            ->join('models', 'vehicles.model_id', '=', 'models.id')
            ->join('body_type', 'vehicles.bodyType_id', '=', 'body_type.id')
            ->join('transmission_type', 'vehicles.transmissionType_id','=','transmission_type.id')
            ->join('fuel_type', 'vehicles.fuelType_id', '=', 'fuel_type.id')
            ->where('vehicles.id', $vehicle)
            ->get();
    }

    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
            'make_id' => 'required|integer',
            'model_id' => 'required|integer',
            'year' => 'required|integer',
            'kilometers' => 'required|integer',
            'bodyType_id' => 'required|integer',
            'engine_size' => 'required|integer',
            'transmissionType_id' => 'required|integer',
            'price' => 'required|integer',
            'description' => 'required|string',
            'power' => 'required|integer',
            'fuelType_id' => 'required|integer'
        ]);

        if($validator->fails()) {
            return response()->json([
                'error' => 'BAD REQUEST',
                'message' => $validator->errors()->toArray()
            ], 400);
        }

        $vehicle = new Vehicle();
        $vehicle->make_id = request('make_id');
        $vehicle->model_id = request('model_id');
        $vehicle->year = request('year');
        $vehicle->kilometers = request('kilometers');
        $vehicle->bodyType_id = request('bodyType_id');
        $vehicle->engine_size = request('engine_size');
        $vehicle->transmissionType_id = request('transmissionType_id');
        $vehicle->price = request('price');
        $vehicle->description = request('description');
        $vehicle->power = request('power');
        $vehicle->fuelType_id = request('fuelType_id');

        $vehicle->save();

        return response()->json([
            'message' => 'Vehicle added successfully!',
            'id' => $vehicle->id,
            $vehicle
        ],201);
    }
}
