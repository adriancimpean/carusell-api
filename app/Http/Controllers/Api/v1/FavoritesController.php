<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Favorite;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;


class FavoritesController extends Controller
{
    public function index() {
        return DB::table('favorites')
                    ->join('users', 'favorites.user_id', '=', 'users.id')
                    ->join('announcements', 'favorites.announcement_id', '=', 'announcements.id')
                    ->select('announcements.*', 'users.*')
                    ->get();
    }

    public function getUserFavorites(int $userId) {
        return DB::table('favorites')
            ->join('users', 'favorites.user_id', '=', 'users.id')
            ->join('announcements', 'favorites.announcement_id', '=', 'announcements.id')
            ->join('vehicles', 'announcements.vehicle_id', '=', 'vehicles.id')
            ->select('announcements.*', 'favorites.announcement_id', 'favorites.id as favorite_id', 'vehicles.price as vehicle_price')
            ->where('users.id', $userId)
            ->get();
    }

    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
            'announcement_id' => 'required|integer',
            'user_id' => 'required|integer'
         ]);

        if($validator->fails()) {
            return response()->json([
                'error' => 'BAD REQUEST',
                'message' => $validator->errors()->toArray()
            ], 400);
        }
 
        $favorite = new Favorite();
        $favorite->announcement_id = request('announcement_id');
        $favorite->user_id = request('user_id');
        $favorite->save();

        return response()->json([
            'message' => 'Announcement marked as favorite!'
        ],200);
    }

    public function delete(int $id) {
        $favorite = Favorite::findOrFail($id);
        if(!$favorite) {
            return response()->json([
                'message' => "Favorite announcement not found!"
            ]);
        }

        $favorite->delete();
        return response()->json([
            'message' => 'Removed from favorites'
        ], 200);

    }
}
