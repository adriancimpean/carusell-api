<?php

namespace App\Http\Controllers\Api\v1\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\User;

class AuthController extends Controller
{
    public function login(Request $request) {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email',
            'password' => 'required|string'
        ]);
        
        if($validator->fails()) {
            return response()->json([
                'error' => 'BAD REQUEST',
                'message' => $validator->errors()->toArray()
            ], 400);
        }
        
        $email = $request->get('email');
        $password = $request->get('password');
        
        if(!Auth::attempt([
            'email' => $email,
            'password' => $password
        ])) {
            return response()->json([
                'error' => 'BAD REQUEST',
                'message' => 'Incorrect email or password'
            ], 400);
        }
        
        $user = $request->user();
        $token = $user -> createToken('Grant Token')->accessToken;
        
        return response()->json([
            'user_id' => $user->id,
            'email' => $user->email,
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'phone' => $user->phone,
            'city_id' => $user->city_id,
            'token' => $token,
            'type' => 'Bearer'
        ], 200);
    }
        
    public function register(Request $request) {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|unique:users',
            'password' => 'required|string|confirmed',
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'phone' => 'required|string',
            'city_id' => 'required|integer'
        ]);
        
        if($validator->fails()) {
            return response()->json([
                'error' => 'BAD REQUEST', 
                'message' => $validator->errors()->toArray(),
                'code' => 400
            ], 400); 
        }
        
        $user = new User([
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'city_id' => $request->city_id,
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'phone' => $request->phone
        ]);
        
        $user->save();
        
        return response()->json([
            'message' => 'Successfully registered!',
            'user' => $user,
            'code' => 201
        ], 201);
    }
        
    public function logout(Request $request) {
        Auth::logout();
        
        return response()->json([
            'message' => 'Successfully logged out!',
            ], 200);
        }
}
