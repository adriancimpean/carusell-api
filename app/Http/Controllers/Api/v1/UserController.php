<?php

namespace App\Http\Controllers\api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function index(Request $request) {
        return DB::table('users')
                    ->join('cities', 'users.city_id', '=', 'city.id')
                    ->select('users.first_name', 'users.last_name', 'users.phone', 'cities.name as city')
                    ->get();
    }

    public function show(int $userId) {
        return DB::table('users')
                    ->join('cities', 'users.city_id', '=', 'cities.id')
                    ->select('users.email', 'users.first_name', 'users.last_name', 'users.phone', 'cities.name as city')
                    ->where('users.id', $userId)
                    ->get();
    }

    public function update(Request $request, $id) {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required|integer',
            'email' => 'required|email',
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'phone' => 'required|string',
            'city_id' => 'required|integer'
         ]);

         if($validator->fails()) {
            return response()->json([
                'error' => 'BAD REQUEST', 
                'message' => $validator->errors()->toArray()
            ], 400); 
        }

        $user = User::findOrFail($id);
        if(!$user) {
            return response()->json([
                'message' => 'User not found',
            ], 404);
        }

        $user->update($request->all());
        return response()->json([
            'message' => 'User successfully updated',
            $user
        ], 200);
    }
}
