<?php
namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Make;

class MakesController extends Controller
{
    public function index(Request $request) {
        return Make::all();
    }
    
    public function show(Make $make) {
        return $make;
    }
}
