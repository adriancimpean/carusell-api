<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Favorite extends Model
{
    use HasFactory;
    protected $table = 'favorites';
    protected $guarded = [];
    public $timestamps = false;

    public function announcement() {
        return $this->belongsTo(Announcement::class);
    }

    public function user() {
        return $this->belongsTo(User::class);
    }
}
