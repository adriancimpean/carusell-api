<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AnnouncementPhotos extends Model
{
    use HasFactory;
    protected $table = 'announcement_photos';
    protected $guarded  = [];
    public $timestamps = false;
    protected $fillable = [
        'announcement_id',
        'photo_url'
    ];

    public function announcement() {
        return $this->belongsTo(Announcement::class);
    }
}
