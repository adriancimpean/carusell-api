<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    use HasFactory;
    protected $table = 'vehicles';
    protected $guarded  = [];
    public $timestamps = false;

    public function make() {
        return $this->belongsTo(Make::class);
    }

    public function model() {
        return $this->belongsTo(Model::class);
    }

    public function bodyType() {
        return $this->belongsTo(BodyType::class);
    }

    public function transmissionType() {
        return $this->belongsTo(TransmissionType::class);
    }

    public function fuelType() {
        return $this->belongsTo(FuelType::class);
    }
}
